import pandas as pd


def get_bigger_serie(A: pd.Series, B: pd.Series):
    return A if A.size >= B.size else B


def generate_csv():
    comunidades = ['Andalucía', 'Aragón', 'Asturias', 'Baleares', 'Canarias', 'Cantabria', 'Castilla La Mancha', 'Castilla y León', 'Cataluña',
                   'Ceuta', 'C. Valenciana', 'Extremadura', 'Galicia', 'Madrid', 'Melilla', 'Murcia', 'Navarra', 'País Vasco', 'La Rioja']
    columnas = ['CCAA', 'FECHA', 'Casos', 'Altas', 'PCR+',
                'TestAc+', 'Hospitalizados', 'UCI', 'Fallecidos']

    url_casos = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_casos_long.csv'
    url_altas = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_altas_long.csv'
    url_pcr = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_confirmados_pcr_long.csv'
    url_fallecidos = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_fallecidos_long.csv'
    url_hospitalizados = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_hospitalizados_long.csv'
    url_uci = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_uci_long.csv'
    url_test = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_confirmados_test_long.csv'

    df_casos = pd.read_csv(url_casos, sep=',')
    df_altas = pd.read_csv(url_altas, sep=',')
    df_pcr = pd.read_csv(url_pcr, sep=',')
    df_fallecidos = pd.read_csv(url_fallecidos, sep=',')
    df_hospitalizados = pd.read_csv(url_hospitalizados, sep=',')
    df_uci = pd.read_csv(url_uci, sep=',')
    df_test = pd.read_csv(url_test, sep=',')

    fechas_casos = df_casos['fecha']
    fechas_altas = df_altas['fecha']
    fechas_pcr = df_pcr['fecha']
    fechas_fallecidos = df_fallecidos['fecha']
    fechas_hospitalizados = df_hospitalizados['fecha']
    fechas_uci = df_uci['fecha']
    fechas_test = df_test['fecha']

    fechas_casos = fechas_casos.drop_duplicates()
    fechas_altas = fechas_altas.drop_duplicates()
    fechas_pcr = fechas_altas.drop_duplicates()
    fechas_fallecidos = fechas_altas.drop_duplicates()
    fechas_hospitalizados = fechas_altas.drop_duplicates()
    fechas_uci = fechas_uci.drop_duplicates()
    fechas_test = fechas_test.drop_duplicates()

    fechas_mayor = get_bigger_serie(fechas_casos,
                                    get_bigger_serie(fechas_altas,
                                                     get_bigger_serie(fechas_pcr, get_bigger_serie(fechas_fallecidos,
                                                                                                   get_bigger_serie(fechas_hospitalizados,
                                                                                                                    get_bigger_serie(fechas_uci, fechas_test))))))

    del fechas_casos
    del fechas_altas
    del fechas_pcr
    del fechas_fallecidos
    del fechas_hospitalizados
    del fechas_uci
    del fechas_test

    temp_casos_list = []
    temp_altas_list = []
    temp_pcr_list = []
    temp_test_list = []
    temp_hospitalizados_list = []
    temp_uci_list = []
    temp_fallecidos_list = []
    temp_comunidades_list = []

    output = pd.DataFrame(columns=columnas)

    for comunidad in comunidades:
        temp_casos_list = []
        temp_altas_list = []
        temp_pcr_list = []
        temp_test_list = []
        temp_hospitalizados_list = []
        temp_uci_list = []
        temp_fallecidos_list = []
        temp_comunidades_list = []

        temp_casos = df_casos[(df_casos.CCAA == comunidad)].rename(
            columns={"total": columnas[2]})
        temp_casos_list += temp_casos[columnas[2]].to_list()

        temp_altas = df_altas[(df_altas.CCAA == comunidad)].rename(
            columns={"total": columnas[3]})
        temp_altas_list += temp_altas[columnas[3]].to_list()

        temp_pcr = df_pcr[(df_pcr.CCAA == comunidad)].rename(
            columns={"total": columnas[4]})
        temp_pcr_list += temp_pcr[columnas[4]].to_list()

        temp_test = df_test[(df_test.CCAA == comunidad)].rename(
            columns={"total": columnas[5]})
        temp_test_list = temp_test[columnas[5]].to_list()

        temp_hospitalizados = df_hospitalizados[(
            df_hospitalizados.CCAA == comunidad)].rename(columns={"total": columnas[6]})
        temp_hospitalizados_list += temp_hospitalizados[columnas[6]].to_list()

        temp_uci = df_uci[(df_uci.CCAA == comunidad)].rename(
            columns={"total": columnas[7]})
        temp_uci_list = temp_uci[columnas[7]].to_list()

        temp_fallecidos = df_fallecidos[(df_fallecidos.CCAA == comunidad)].rename(
            columns={"total": columnas[8]})
        temp_fallecidos_list = temp_fallecidos[columnas[8]].to_list()

        for index in range(fechas_mayor.size):
            temp_comunidades_list.append(comunidad)

        temp_df = pd.DataFrame.from_dict({'CCAA': pd.Series(temp_comunidades_list), 'FECHA': pd.Series(fechas_mayor), 'Casos': pd.Series(temp_casos_list)
                                      , 'Altas': pd.Series(temp_altas_list), 'PCR+': pd.Series(temp_pcr_list), 'TestAc+': pd.Series(temp_test_list),
                                      'Hospitalizados': pd.Series(temp_hospitalizados_list), 'UCI': pd.Series(temp_uci_list),
                                      'Fallecidos': pd.Series(temp_fallecidos_list)})
    
        output = output.append(temp_df, ignore_index=True)
    
    output = output.ffill()
        

    del df_casos
    del temp_casos

    del df_altas
    del temp_altas

    del df_pcr
    del temp_pcr

    del df_test
    del temp_test

    del df_hospitalizados
    del temp_hospitalizados

    del df_uci
    del temp_uci

    del df_fallecidos
    del temp_fallecidos

    output.to_csv('./volumes/www/agregados.csv', index=False, sep=',', header=True)


if __name__ == "__main__":
    generate_csv()
