package Analisis_COVID_19
import scala.collection.mutable._
import scala.io.StdIn._


object COMMUNITIES {
  private val communities = Array("Andalucía", "Aragón", "Asturias", "Baleares", "Canarias",
    "Cantabria", "Castilla La Mancha", "Castilla y León", "Cataluña", "Ceuta", "C. Valenciana", "Extremadura",
    "Galicia", "Madrid", "Melilla", "Murcia", "Navarra", "País Vasco", "La Rioja")
  private var selectedCommunities: ArrayBuffer[String] = new ArrayBuffer[String]()
  private var tempCommunities = ArrayBuffer(communities:_*)

  def selectCommunities(): ArrayBuffer[String] =  {
    var selection = "_"
    print("Selecciona al menos una comunidad autónoma\n")
    var continue = true
    while ((selection != "C" && selection != "T") || selectedCommunities.isEmpty && continue) {
      println("\n\nSelecciona la comunidad que quieras comprobar")
      print_autonomous_communities()
      selection = readLine().toUpperCase()
      if (isNumeric(selection))
        this.addToSelectedList(selection.toInt)
      else
        if(selection.toUpperCase() == "T") {
          this.selectedCommunities.addAll(this.tempCommunities)
          this.tempCommunities.clear()
        }
        else if (selection != "C" && selection != "c") {
          print("El elemento escogido es incorrecto. Por favor selecciona un elemento de la lista\n\n\n")
        }
      if (this.tempCommunities.isEmpty)
        continue = false
    }
    return this.selectedCommunities
  }

  def isNumeric(x: String) : Boolean = {
    var is_numeric = true
    if(x.length > 1) for(i<-0 to x.length-1) {if (!x.charAt(i).isDigit) is_numeric = false}
    else is_numeric = x.charAt(0).isDigit
    is_numeric
  }

  def print_autonomous_communities(): Unit = {
    for (i<-tempCommunities.indices) print("["+(i+1)+"] "+tempCommunities(i)+"\n")
    print("T añade todas las comunidades\nSelecciona C para mostrar los resultados\n")
  }

  private def addToSelectedList(selection: Int) {
    if (tempCommunities.size >= selection) selectedCommunities.addOne(tempCommunities.remove(selection - 1))
    else print("El elemento escogido es incorrecto. Por favor selecciona un elemento de la lista\n")
  }

  def clearLists() {
    selectedCommunities = new ArrayBuffer[String]()
    tempCommunities = ArrayBuffer(communities:_*)
  }
}