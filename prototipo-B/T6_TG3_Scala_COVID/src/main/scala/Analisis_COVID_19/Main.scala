package Analisis_COVID_19

object Main {
  def main(args: Array[String]): Unit = {
    print("Bienvenido al generador de gráficos y datos del COVID-19 en España\n")
    while (true) {
      USER_GRAPHICS.askForGraphics() match {
        case "1" => CSV_COVID_FILE.getWhatToCalculate(false)
        case "2" => {
          USER_GRAPHICS.whatGraphics() match{
            case "1" => CSV_COVID_FILE.getWhatToCalculate(true)
            case "2" => CSV_COVID_FILE.getWhereToShowGraphics()
            case "S" => print("Menú anterior\n")
          }
        }
      }
    }
  }
}