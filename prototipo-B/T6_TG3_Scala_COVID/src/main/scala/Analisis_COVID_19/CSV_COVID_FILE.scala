package Analisis_COVID_19

import java.net.URL
import java.time.LocalDate
import java.util

import tech.tablesaw.aggregate.AggregateFunctions.{mean, sum}
import tech.tablesaw.api._
import tech.tablesaw.io.csv.CsvReadOptions
import tech.tablesaw.api.ColumnType._
import tech.tablesaw.plotly.Plot
import tech.tablesaw.plotly.api.{HorizontalBarPlot, TimeSeriesPlot}

import scala.collection.mutable.ArrayBuffer
import scala.io.StdIn.readLine

object CSV_COVID_FILE {

  private val location = "https://daniel.halogenos.org/agregados.csv"
  private val covid_table = Table.read.csv(CsvReadOptions.builder(new URL(location)).columnTypes(Array[ColumnType](STRING, LOCAL_DATE, DOUBLE, DOUBLE, DOUBLE, DOUBLE, DOUBLE, DOUBLE, DOUBLE)))
  private val table_with_variations = load_csv_with_variations()
  private val communities = COMMUNITIES

  def load_csv_without_variations(): Table = covid_table

  def load_csv_with_variations(): Table = {
    var table_with_variations = Table.create("COVID-19", covid_table.stringColumn(0), covid_table.dateColumn(1))

    val column_Names = Array("Variaciones Casos", "Variaciones Altas", "Variaciones PCR", "Variaciones TestAc+",
      "Variaciones Hospitalizados", "Variaciones UCI", "Variaciones Fallecidos")

    val initialDate = LocalDate.parse("2020-02-21")

    for ((columnName, index) <- column_Names.view.zipWithIndex) {
      var variations_list = new Array[Number](covid_table.rowCount())
      table_with_variations.addColumns(covid_table.doubleColumn(index + 2))
      table_with_variations.addColumns(create_column(variations_list, initialDate, index, columnName))
    }

    table_with_variations
  }

  private def create_column(numbers: Array[Number], date: LocalDate, i: Int, str: String): DoubleColumn = {
    for (j <- 0 to covid_table.rowCount() - 1) {

      if (covid_table.dateColumn(1).get(j).compareTo(date) == 0) numbers(j) = 0
      else numbers(j) = covid_table.doubleColumn(i + 2).get(j) - covid_table.doubleColumn(i + 2).get(j - 1)
    }
    DoubleColumn.create(str, numbers)
  }

  def last_update_date(): LocalDate = covid_table.dateColumn(1).get(covid_table.rowCount() - 1)

  def generate_national_table(first_date: LocalDate, final_date: LocalDate, decision: Int): Table = {
    var finalTable = this.table_with_variations.where(this.table_with_variations.dateColumn("FECHA").isBetweenIncluding(first_date, final_date))
    decision match {
      case 3 => {
        finalTable.setName("Total")
        finalTable = summarizingData(finalTable)
      }
      case 2 => {
        finalTable.setName("Max")
        finalTable = generateMaxCountry(finalTable)
      }
      case _ => {
        finalTable.setName("Average")
        finalTable = averageData(finalTable)
      }
    }
    finalTable
  }

  private def summarizingData(t: Table): Table = t.summarize("Variaciones Casos", "Variaciones Altas", "Variaciones PCR", "Variaciones TestAc+",
    sum).apply().concat(t.summarize("Variaciones Hospitalizados", "Variaciones UCI", "Variaciones Fallecidos", sum).apply())


  private def generateMaxCountry(t: Table): Table = {
    val finalTable = Table.create("Máximos Nacionales", DateColumn.create("FECHA"), StringColumn.create("Campo"), DoubleColumn.create("Máximo"))
    var auxTable = t.copy()
    var c = 1
    while (c < 13) {
      c += 2
      val columnName = t.column(c).name()
      auxTable = auxTable.sortDescendingOn(columnName)
      finalTable.column("FECHA").appendObj(auxTable.row(0).getDate("FECHA"))
      finalTable.column("Campo").appendObj(columnName)
      finalTable.column("Máximo").appendObj(auxTable.row(0).getDouble(columnName))
    }
    finalTable
  }

  private def averageData(t: Table): Table = t.summarize("Variaciones Casos", "Variaciones Altas",
    "Variaciones PCR", "Variaciones TestAc+", mean).apply().concat(t.summarize("Variaciones Hospitalizados",
    "Variaciones UCI", "Variaciones Fallecidos", mean).apply())

  private def generateMaxCommunity(t: Table): Table = {
    val finalTable = Table.create("Máximos entre comunidades", StringColumn.create("CCAA"),
      DateColumn.create("FECHA"), StringColumn.create("Campo"), DoubleColumn.create("Máximo"))
    var auxTable = t.copy()
    for (c <- 2 until 16) {
      if (c % 2 != 0) {
        val columnName = t.column(c).name()
        auxTable = auxTable.sortDescendingOn(columnName)
        finalTable.column("CCAA").appendObj(auxTable.row(0).getString("CCAA"))
        finalTable.column("FECHA").appendObj(auxTable.row(0).getDate("FECHA"))
        finalTable.column("Campo").appendObj(columnName)
        finalTable.column("Máximo").appendObj(auxTable.row(0).getDouble(columnName))
      }
    }
    finalTable
  }

  private def generateCommunityTable(communityList: ArrayBuffer[String], first_date: LocalDate, final_date: LocalDate, decision: Int): Table = {
    val variations = Array("Variaciones Casos", "Variaciones Altas", "Variaciones PCR", "Variaciones TestAc+",
      "Variaciones Hospitalizados", "Variaciones UCI", "Variaciones Fallecidos")
    val arr: util.ArrayList[String] = new util.ArrayList[String]()
    for (i <- communityList) arr.add(i)

    var finalTable = decision match {
      case 3 => {
        val res = Table.create("Totales entre comunidades")
        for (i <- variations.indices) res.addColumns(DoubleColumn.create("Sum [" + variations(i) + "]"))
        res
      }

      case 2 => {
        Table.create("Máximos entre comunidades", StringColumn.create("CCAA"),
          DateColumn.create("FECHA"), StringColumn.create("Campo"), DoubleColumn.create("Máximo"))
      }
      case _ => {
        val res = Table.create("Media entre comunidades")
        for (i <- variations.indices) res.addColumns(DoubleColumn.create("Mean [" + variations(i) + "]"))
        res
      }

    }
    for (c <- communityList) {
      var auxTable = table_with_variations.where(table_with_variations.stringColumn("CCAA").isEqualTo(c))
      auxTable = auxTable.where(auxTable.dateColumn("FECHA").isBetweenIncluding(first_date, final_date))

      decision match {
        case 3 => finalTable.append(summarizingData(auxTable))

        case 2 => {
          if (communityList.size > 1)
            finalTable = compareAndGetBigger(finalTable, generateMaxCommunity(auxTable))
          else
            finalTable.append(generateMaxCommunity(auxTable))
        }

        case _ => finalTable.append(averageData(auxTable))
      }
    }

    if (decision != 2) finalTable.addColumns(StringColumn.create("CCAA", arr))

    finalTable
  }

  private def compareAndGetBigger(t1: Table, t2: Table): Table = {
    val fields = Array("CCAA", "FECHA", "Campo", "Máximo")
    if (t1 != null) {
      if (t1.column(0).size() > 0)
        if (t2 != null) {
          return if (t2.column(0).size() > 0) {
            val tempTable = Table.create("Máximos entre comunidades", StringColumn.create("CCAA"),
              DateColumn.create("FECHA"), StringColumn.create("Campo"), DoubleColumn.create("Máximo"))
            for (index <- 0 until 7) {
              if (t1.row(index).getDouble("Máximo") > t2.row(index).getDouble("Máximo")) {
                tempTable.column("CCAA").appendObj(t1.row(index).getString("CCAA"))
                tempTable.column("FECHA").appendObj(t1.row(index).getDate("FECHA"))
                tempTable.column("Campo").appendObj(t1.row(index).getString("Campo"))
                tempTable.column("Máximo").appendObj(t1.row(index).getDouble("Máximo"))
              } else {
                tempTable.column("CCAA").appendObj(t2.row(index).getString("CCAA"))
                tempTable.column("FECHA").appendObj(t2.row(index).getDate("FECHA"))
                tempTable.column("Campo").appendObj(t2.row(index).getString("Campo"))
                tempTable.column("Máximo").appendObj(t2.row(index).getDouble("Máximo"))
              }
            }
            tempTable
          } else
            t1
        } else return null
    }
    if (t2 != null)
      if (t2.column(0).size() > 0)
        return t2
    null
  }

  def askWhatToShow(): Int = {
    print("¿Qué datos te gustaría ver?\n[1] Media\t[2] Máximo\t[3] Totales\n>> ")
    var answer = "_"
    while (answer.toUpperCase() != "S") {
      answer = readLine
      if (answer == "1" || answer == "2" || answer == "3") return answer.toInt
      else print("El valor introducido no es correcto, vuelve a intentarlo\n>> ")
    }
    -1
  }

  private def checkDateIsCorrect(dateToCheck: LocalDate, firstDate: LocalDate): Boolean = {
    val minDate = this.table_with_variations.row(0).getDate("FECHA")
    val maxDate = this.table_with_variations.row(this.table_with_variations.rowCount() - 1).getDate("FECHA")
    if (firstDate != null) {
      if (dateToCheck.getDayOfYear < firstDate.getDayOfYear) return false
    }
    if (dateToCheck.getDayOfYear < minDate.getDayOfYear) return false
    if (dateToCheck.getDayOfYear > maxDate.getDayOfYear) return false
    true
  }

  private def selectDate(needsTwoDates: Boolean, isSecondDate: Boolean, firstDate: LocalDate): LocalDate = {
    var finish = false
    var date: LocalDate = LocalDate.now()
    while (!finish) {
      if (needsTwoDates) {
        if (isSecondDate) {
          try {
            print("Introduce una fecha final para el intervalo con formato mes-dia MM-DD (Ejemplo: 05-25)\n>> ")
            date = LocalDate.parse("2020-" + readLine)
            if (checkDateIsCorrect(date, firstDate)) finish = true
          } catch {
            case e: Exception => print("El valor introducido no es una fecha\n")
          }
        } else {
          try {
            print("Introduce una fecha inicial para el intervalo con formato mes-dia MM-DD (Ejemplo: 05-25)\n>> ")
            date = LocalDate.parse("2020-" + readLine)
            if (checkDateIsCorrect(date, null)) finish = true
          } catch {
            case e: Exception => print("El valor introducido no se reconoce como fecha\n")
          }
        }
      } else {
        try {
          print("Introduce una fecha con formato mes-dia MM-DD (Ejemplo: 05-25)\n>> ")
          date = LocalDate.parse("2020-" + readLine)
          if (checkDateIsCorrect(date, null)) finish = true
        } catch {
          case e: Exception => print("El valor introducido no se reconoce como fecha\n")
        }
      }
    }
    date
  }

  private def askDate(): String = {
    print("¿Quieres seleccionar un periodo de tiempo o una fecha específica?\n1 => Periodo de tiempo\n2 => Fecha especifica\n3 => Todos los registros\n>> ")
    readLine()
  }

  private def getDates(whatType: Int): Array[LocalDate] = {
    var initialDate: LocalDate = LocalDate.now()
    var finalDate: LocalDate = LocalDate.now()
    whatType match {
      case 1 => {
        initialDate = selectDate(needsTwoDates = true, isSecondDate = false, firstDate = null)
        finalDate = selectDate(needsTwoDates = true, isSecondDate = true, firstDate = initialDate)
      }
      case 2 => {
        initialDate = selectDate(needsTwoDates = false, isSecondDate = false, firstDate = null)
        finalDate = initialDate
      }
      case _ => {
        initialDate = this.table_with_variations.row(0).getDate("FECHA")
        finalDate = this.table_with_variations.row(this.table_with_variations.rowCount() - 1).getDate("FECHA")
      }
    }
    return Array[LocalDate](initialDate, finalDate)
  }

  private def askForWhat(): Int = {
    print("¿Qué datos te gustaría ver?\n[1] Nacionales\n[2] Comunidades\n[3] Nacionales VS Comunidades\n>> ")
    var answer = "_"
    var control = "_"
    while (control.toUpperCase() != "S") {
      answer = readLine
      if (answer == "1" || answer == "2" || answer == "3") return answer.toInt
      else print("El valor introducido no es válido\n")
    }
    return answer.toInt
  }

  def getWhereToShowGraphics(): Unit = {
    val dataToShow = askForWhat()
    val arrayDates = getDates(askDate().toInt)
    val field = askForElement()
    timeSerieGraphic(getTableForGraphic(dataToShow, arrayDates(0), arrayDates(1)), field)
  }

  private def askForElement(): String = {
    val values = Array("Casos", "Altas", "PCR+", "TestAc+", "Hospitalizados",
      "UCI", "Fallecidos")
    val number_values = List("1", "2", "3", "4", "5", "6", "7")
    var answer = "C"
    print("\n¿Que elemento quieres dibujar?\n")
    while (answer != "S") {
      for ((value, index) <- values.view.zipWithIndex) print("[" + (index + 1) + "]" + " " + value + "\n")
      print(">> ")
      answer = readLine
      if (number_values.contains(answer)) return answer
      else print("El valor introducido no es correcto\n")
    }
    answer
  }

  private def timeSerieGraphic(t: Table, field: String): Unit = {
    val columnValues = Array("Casos", "Altas", "PCR+", "TestAc+", "Hospitalizados", "UCI", "Fallecidos")
    Plot.show(
      TimeSeriesPlot.create(columnValues(field.toInt - 1) + " in a time", t,
        "FECHA", columnValues(field.toInt - 1), "CCAA"))
  }

  private def getTableForGraphic(typeData: Int, initialDate: LocalDate, finalDate: LocalDate): Table = {
    typeData match {
      case 1 => generateCountryTimeSerieGraphic(initialDate, finalDate)
      case 2 => {
        val list = this.communities.selectCommunities()
        generateCommunityTimeSerieGraphic(list, initialDate, finalDate)
      }
      case _ => {
        val list = this.communities.selectCommunities()
        generateCommunityTimeSerieGraphic(list, initialDate, finalDate).append(generateCountryTimeSerieGraphic(initialDate, finalDate))
      }
    }
  }

  private def generateCommunityTimeSerieGraphic(communityList: ArrayBuffer[String], initial_date: LocalDate, final_date: LocalDate): Table = {
    var tableForGraphics = covid_table.emptyCopy()
    for (c <- communityList) {
      var auxTable = this.covid_table.where(this.covid_table.stringColumn("CCAA").isEqualTo(c))
      auxTable = auxTable.where(auxTable.dateColumn("FECHA").isBetweenIncluding(initial_date, final_date))
      tableForGraphics = tableForGraphics.append(auxTable)
    }
    return tableForGraphics
  }

  private def generateCountryTimeSerieGraphic(initialDate: LocalDate, finalDate: LocalDate): Table = {

    val columnValues = Array("Casos", "PCR+", "TestAc+", "Altas", "Hospitalizados", "Fallecidos", "UCI")

    var auxTable = this.covid_table.where(this.covid_table.dateColumn("FECHA").isBetweenIncluding(initialDate, finalDate))
    var tableForGraphics = auxTable.summarize("Casos", "Altas", "PCR+", "TestAc+", sum).by("FECHA")
    auxTable = auxTable.summarize("Hospitalizados",
      "UCI", "Fallecidos", sum).by("FECHA")
    tableForGraphics.sortOn("FECHA")
    auxTable.sortOn("FECHA")
    auxTable.removeColumns("FECHA")
    tableForGraphics = tableForGraphics.concat(auxTable)
    val values: util.ArrayList[String] = new util.ArrayList()

    for (i <- 0 until tableForGraphics.rowCount()) values.add("Nacional")

    tableForGraphics.addColumns(StringColumn.create("CCAA", values))

    for (c <- 1 until 8) {
      tableForGraphics.column(c).setName(columnValues(c - 1))
    }
    tableForGraphics
  }

  private def drawBarPlot(t: Table, whatToShow: Int, field: String) = {
    val values = Array("Variaciones Casos", "Variaciones Altas", "Variaciones PCR", "Variaciones TestAc+", "Variaciones Hospitalizados",
      "Variaciones UCI", "Variaciones Fallecidos")
    var textTitle: String = "_"
    var column: String = "_"
    whatToShow match {
      case 1 => {
        textTitle = "Media de " + values(field.toInt - 1)
        column = "mean " + "["+values(field.toInt - 1)+"]"
      }
      case 2 => {
        textTitle = "Máximo de " + values(field.toInt - 1)
        column = "Máximo"
      }
      case _ => {
        textTitle = "Total de " + values(field.toInt - 1)
        column = "Sum " + "["+values(field.toInt - 1)+"]"
      }
    }

    Plot.show(HorizontalBarPlot.create(textTitle, t, "CCAA", column))
  }

  def getWhatToCalculate(isGraphics: Boolean) = {
    val community:Int = askForWhat()
    val whatToCalculate = askWhatToShow()
    val arrayDates = getDates(askDate().toInt)

    community match {
      case 1 => {
        if (!isGraphics)
          print(generate_national_table(arrayDates(0), arrayDates(1), whatToCalculate))
        else {
          val tableToPlot = generate_national_table(arrayDates(0), arrayDates(1), whatToCalculate)
          val values: util.ArrayList[String] = new util.ArrayList[String]()
          for (r<-0 until tableToPlot.rowCount()) {
            values.add("Nacional")
          }
          tableToPlot.addColumns(StringColumn.create("CCAA", values))

          // Plot tableToPlot
          drawBarPlot(tableToPlot, whatToCalculate, askForElement())
        }
      }
      case 2 => {
        val selectedCommunities = this.communities.selectCommunities()
        if (!isGraphics)
          println(generateCommunityTable(selectedCommunities, arrayDates(0), arrayDates(1), whatToCalculate))
        else {
          val tableToPlot = generateCommunityTable(selectedCommunities, arrayDates(0), arrayDates(1), whatToCalculate)
          // Plot tableToPlot
          drawBarPlot(tableToPlot, whatToCalculate, askForElement())
        }
        this.communities.clearLists()

      }
      case 3 => {
        val selectedCommunities = this.communities.selectCommunities()
        val tempTable = generate_national_table(arrayDates(0), arrayDates(1), whatToCalculate)

        val columna = if (whatToCalculate != 2) {
          val col = StringColumn.create("CCAA", "Nacional")
          col
        } else {
          val values: util.ArrayList[String] = new util.ArrayList[String](util.Arrays.asList("Nacional", "Nacional", "Nacional", "Nacional", "Nacional",
            "Nacional", "Nacional"))

          val col = StringColumn.create("CCAA", values)
          col
        }
        tempTable.addColumns(columna)
        tempTable.append(generateCommunityTable(selectedCommunities, arrayDates(0), arrayDates(1), whatToCalculate))
        if (tempTable != null) {
          if (!isGraphics)
            println(tempTable.printAll())
          else {
            drawBarPlot(tempTable, whatToCalculate, askForElement())
          }
        }
        this.communities.clearLists()
      }
    }
  }
}