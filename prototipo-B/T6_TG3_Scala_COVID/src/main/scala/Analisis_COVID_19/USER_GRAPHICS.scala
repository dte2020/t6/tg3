package Analisis_COVID_19

import scala.io.StdIn.readLine

object USER_GRAPHICS {
  def whatGraphics(): String = {
    println("¿Qué datos te gustaría ver?\n")
    var answer = "C"
    while (answer.toUpperCase() != "S") {
      print("[1] Totales, máximos o medias nacionales y/o de comunidades\n[2] Progresión en días de comunidades y/o nacionales\n[S] Volver al menú anterior\n>> ")
      answer = readLine.toUpperCase
      if (answer == "1" || answer == "2" || answer == "S") return answer
      else println("El valor no se ha reconocido\n")
    }
    return answer
  }

  def askForGraphics(): String = {
    var answer = "C"
    while (answer.toUpperCase() != "S") {
      print("\n\n¿Que te gustaría hacer?\n[1] Obtener datos numéricos\n[2] Obtener gráficas a partir de datos\n[S] Salir del programa\n>> ")
      answer = readLine.toUpperCase()
      if (answer == "1" || answer == "2") return answer
      else if (answer == "S") System.exit(0)
      else print("El valor introducido no es correcto\n")
    }
    answer
  }
}
